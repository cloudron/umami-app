#!/bin/bash

set -eu

[[ ! -f /app/data/env.sh ]] && sed -e "s,HASH_SALT=.*,HASH_SALT=$(pwgen -1s 32)," /app/pkg/env.sh.template > /app/data/env.sh

echo "==> Changing ownership"
chown -R cloudron:cloudron /app/data

# standard env vars
export NODE_ENV=production
export DATABASE_URL=${CLOUDRON_POSTGRESQL_URL}
export FORCE_SSL=1
export PORT=3000
export DISABLE_UPDATES=1
export DATABASE_TYPE=postgresql # only used for yarn build

# source it before the build, lets one set COLLECT_API_ENDPOINT (https://github.com/umami-software/umami/issues/1318)
source /app/data/env.sh

# https://github.com/umami-software/umami/issues/1583
PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "CREATE EXTENSION IF NOT EXISTS pgcrypto;"

# this tramples a whole lot of code directories listed in the manifest. setting VERCEL=1 skips build-geo
echo "=> Running build script that generates the migrations"
VERCEL=1 yarn run build

echo "=> Running migrations"
gosu cloudron:cloudron yarn run update-db

echo "==> Starting Umami"
exec gosu cloudron:cloudron yarn next start

