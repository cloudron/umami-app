FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# https://github.com/umami-software/umami/blob/master/Dockerfile#L2
RUN node -v | grep -q 'v22' || (echo "Requires node v22" && exit 1)

ENV NEXT_TELEMETRY_DISABLED 1

# v2.16 made us change the versioning from semver to loose
# renovate: datasource=github-releases depName=umami-software/umami versioning=loose extractVersion=^v(?<version>.+)$
ARG UMAMI_VERSION=2.17.0

RUN curl -L https://github.com/umami-software/umami/archive/refs/tags/v${UMAMI_VERSION}.tar.gz | tar zxvf - --strip-components 1
RUN npm install -g yarn && \
    yarn install && \
    yarn run build-geo && \
    yarn cache clean

RUN ln -s /app/data/geo /app/code/geo

COPY env.sh.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
