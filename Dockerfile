FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

ENV NEXT_TELEMETRY_DISABLED 1

# v2.16 made us change the versioning from semver to loose
# renovate: datasource=github-releases depName=umami-software/umami versioning=loose extractVersion=^v(?<version>.+)$
ARG UMAMI_VERSION=2.16.1

RUN curl -L https://github.com/umami-software/umami/archive/refs/tags/v${UMAMI_VERSION}.tar.gz | tar zxvf - --strip-components 1 && \
    yarn install && \
    yarn run build-geo && \
    yarn cache clean

RUN ln -s /app/data/geo /app/code/geo

COPY env.sh.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
